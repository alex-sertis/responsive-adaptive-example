import React, {useState} from 'react'
import DisplayText, {DisplayTextSize} from "./components/display-text";
import MediaQuery from 'react-responsive'
import { MediaQuerySize } from './global-constant';
import './App.css';

const App = () => {
  const [count,setCount] = useState(0);

  const renderText = (text) => (<span style={{fontWeight:'bold', fontSize: '20px'}}>{text}</span>);

  return (
    <div className="App">
      <button style={{width: '200px', height:'100px', alignSelf:'center'}} onClick={() => { setCount(count+1) }}>Add Count</button>
      <MediaQuery {...MediaQuerySize.Large}>
        {renderText('Large')}
        <DisplayText size={DisplayTextSize.Large} data={count}/>
      </MediaQuery>
      <MediaQuery {...MediaQuerySize.Medium}>
        {renderText('Medium')}
        <DisplayText size={DisplayTextSize.Medium} data={count}/>
      </MediaQuery>
      <MediaQuery {...MediaQuerySize.Small}>
        {renderText('Small')}
        <DisplayText size={DisplayTextSize.Small} data={count}/>
      </MediaQuery>
    </div>
  );
}

export default App;
