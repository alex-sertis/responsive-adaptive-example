export const MediaQuerySize = {
  Large: {
    minWidth: 1224,
  },
  Medium: {
    maxWidth: 1224,
    minWidth: 768,
  },
  Small: {
    maxWidth: 768,
  },
  MediumSmall: {
    maxWidth: 1224,
  }
};