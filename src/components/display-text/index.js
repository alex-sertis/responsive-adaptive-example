import React from 'react';
import './style.css';

const DisplayText = ({size, data}) => {

  const renderLarge = (data) => (
    <div style={{display:'flex', flexGrow:'1'}}>
      <div style={{flexGrow: 1, backgroundColor: 'green'}}>Flex 1 {data}</div>
      <div style={{flexGrow: 3, backgroundColor: 'yellow'}}>Flex 2 {data}</div>
      <div style={{flexGrow: 1, backgroundColor: 'red'}}>Flex 3 {data}</div>
    </div>
  );

  const renderMedium = (data) => (
    <div style={{display:'flex', flexDirection:'column', flexGrow:'1'}}>
      <div style={{display:'flex', height: '20%'}}>
        <div style={{flexGrow: 1, height: '100%', backgroundColor: 'green'}}>Flex 1 {data}</div>
        <div style={{flexGrow: 1, height: '100%', backgroundColor: 'red'}}>Flex 3 {data}</div>
      </div>
      <div style={{flexGrow: 3, height: '100%', backgroundColor: 'yellow'}}>Flex 2 {data}</div>
    </div>
  );
  const renderSmall = (data) => (
    <div style={{display:'flex', flexDirection: 'column', flexGrow: '1'}}>
      <div style={{flexGrow: 1, height: '100%', backgroundColor: 'green'}}>Flex 1 {data}</div>
      <div style={{flexGrow: 1, height: '100%', backgroundColor: 'yellow'}}>Flex 2 {data}</div>
      <div style={{flexGrow: 1, height: '100%', backgroundColor: 'red'}}>Flex 3 {data}</div>
    </div>
  );


  return (
    <div className="displat-text">
      { size === DisplayTextSize.Large && renderLarge(data) }
      { size === DisplayTextSize.Medium && renderMedium(data) }
      { size === DisplayTextSize.Small && renderSmall(data)}
    </div>
  );
}

export const DisplayTextSize = {
  Large: 'large',
  Medium: 'medium',
  Small: 'small',
}

export default DisplayText;
